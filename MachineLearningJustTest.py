 #!/usr/bin/env python2
# -*- coding: utf-8 -*-


from sklearn import svm
#from sklearn.preprocessing import scale
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
import matplotlib.pyplot as plt
import numpy as np
import myfunctions as mfct
import myplot as mplt
import mymachinelearning as mml
from astropy.table import Table
from mpl_toolkits.mplot3d import Axes3D

'''
This function is written to classify unclassified sources and not to
find good train parameters
'''

# close open figures
plt.close('all')
# all sources to train
trainrate = 1


# Load Photometrie Data
cluster_fit = np.loadtxt('PhotometryFitCluster.txt')[:,2:]
compact_fit = np.loadtxt('PhotometryFitCompactsourceRANDOM.txt')[:,2:]
random_fit = np.loadtxt('PhotometryFitRandomsource_long.txt')[:,2:]
testdata = np.loadtxt('PhotometryFitClusterCandidate.txt')
testdata = np.loadtxt('FitResults/PhotometryFitCandidateAurelienSamuel.txt')
# split in location and photometrie data -> localization of confirmed sources
# possible
CC_fit = testdata[:,2:]
testloc = testdata[:,0:2]

# Prepare Data (see mymachinelearning module)
traindat, traintype, valdat, valtype, checkdat = mml.PrepareML([cluster_fit, compact_fit, random_fit],
                                                               checkdata = CC_fit)
    
# choose method and create classifier object
'''
clf = svm.SVC(C=200.0, cache_size=200, class_weight=None, coef0=0.0,
      decision_function_shape='ovr', degree=3, gamma='auto', kernel='rbf',
      max_iter=-1, probability=True, random_state=None, shrinking=True,
      tol=0.001, verbose=False)
'''
clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 4), random_state=1)

#clf = RandomForestClassifier(max_depth=100, random_state=0)

'''
clf =svm.SVC(C=1.0, kernel='rbf', degree=3, gamma='auto', coef0=0.0,
          shrinking=True, probability=False, tol=0.001, cache_size=200,
          class_weight=None, verbose=False, max_iter=-1,
          decision_function_shape='ovr', random_state=None)
'''

# start learn process
clf.fit(traindat, traintype)  
# predict train data to verify if learning process worked
traincheck = clf.predict(traindat)
# predict data to classify
ccclass = clf.predict(checkdat)
# predict with propability
probclass = clf.predict_proba(checkdat)

# evaluate data
trainmat = mml.TransitionMatrix(traintype, traincheck)
candidate =mml.ClassificationResult(ccclass, np.array((0,1,2)))


# plot trainmatrix
plt.figure()
plt.title('Train Check', fontsize=30)
plt.imshow(trainmat)
mplt.MyBoxText(trainmat*100.)
mplt.MyTransitionMatrix()
plt.show()

boxprops=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=.3', alpha=.5)
contentboxprops=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=.3', alpha=1)

# plot classified data
barwidth = .8
plt.figure(figsize=(8,8))
#plt.title('Official Cluster Candidates', fontsize=30)
plt.title('Cluster Candidates Group 1.5', fontsize=30)
plt.bar([0,1,2], candidate, width=barwidth)
plt.xticks([0,1,2],['Cluster', 'Compact', 'Random'], fontsize=27)
plt.ylabel('counts', fontsize=28)
#plt.yticks(fontsize=25)
mytext='Entries:%d'%np.sum(candidate)
numberheight = 30
ax=plt.gca()
plt.text(0.98, 0.95, mytext, fontsize=25,horizontalalignment='right', bbox=boxprops,
         verticalalignment='top', transform=ax.transAxes)

text1 = r'$%.1f$'%candidate[0]
text2 = r'$%.1f$'%candidate[1]
text3 = r'$%.1f$'%candidate[2]

plt.text(0,numberheight,text1, fontsize=25, bbox=contentboxprops,
         horizontalalignment='center', verticalalignment='bottom')
#ax.fill_between([0-barwidth/2.,0.4+barwidth/2.], 50,60, alpha=.5)
plt.text(1,numberheight,text2, fontsize=25, bbox=contentboxprops,
         horizontalalignment='center', verticalalignment='bottom')
plt.text(2,numberheight,text3, fontsize=25, bbox=contentboxprops,
         horizontalalignment='center', verticalalignment='bottom')
plt.yticks(fontsize=25)
plt.subplots_adjust(left=.15)
plt.show()

probablyclusters = ccclass == 0
probablylocs = testloc[probablyclusters]
outputtable = Table(probablylocs, names=('GLON', 'GLAT'))#, meta={'Cluster Candidates':'Machine learning confirmed cluster candidates'})
#outputtable.write('Candidatresults/MLConfirmedOfficialCandidats.fits', overwrite=True)

probablynotclusters = np.invert(probablyclusters)
probablynotlocs = testloc[probablynotclusters]
outputnottable = Table(probablynotlocs, names=('GLON', 'GLAT'))
#outputtable.write('Candidatresults/MLRejectedOfficialCandidats.fits', overwrite=True)


probclass = np.transpose(probclass)

if(1):
    mytext='Entries=%d'%len(probclass[0])

    plt.figure()
    plt.hist(probclass[0])
    plt.title('Classification Quality', y=1.02, fontsize=30)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.xlabel('Probability(Cluster)', fontsize=25)
    plt.ylabel('Counts', fontsize=25)
    plt.text(0.5, 0.9, mytext, fontsize=25,horizontalalignment='right', bbox=boxprops,
         verticalalignment='top', transform=plt.gca().transAxes)

    #, probclass[1])
    plt.show()
    

if(0):#plot 3d
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    ax.scatter(probclass[0], probclass[1], probclass[2], zdir='z', s=20, c=None, depthshade=True)
    plt.show()
