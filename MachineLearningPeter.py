 #!/usr/bin/env python2
# -*- coding: utf-8 -*-


from sklearn import svm
#from sklearn.preprocessing import scale
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
import matplotlib.pyplot as plt
import numpy as np
import myfunctions as mfct
import myplot as mplt
import mymachinelearning as mml
from astropy.table import Table
from astropy.io import fits


plt.close('all')

# how many runs
NRUNS = 5
trainrate = 0.8


# Load Photometrie Data
cluster_fit = np.loadtxt('PhotometryFitCluster.txt')[:,2:]
compact_fit = np.loadtxt('PhotometryFitCompactsourceRANDOM.txt')[:,2:]
random_fit = np.loadtxt('PhotometryFitRandomsource_long.txt')[:,2:]
testdata = np.loadtxt('PhotometryFitClusterCandidate.txt')
#testdata = np.loadtxt('FitResults/PhotometryFitCandidateAurelienSamuel.txt')
# split in location and photometrie data -> localization of confirmed sources
# possible
CC_fit = testdata[:,2:]
testloc = testdata[:,0:2]

# create arrays to save result of each run in another layer
trainstack = np.zeros((NRUNS, 3, 3))
valstack = np.zeros((NRUNS, 3, 3))
candidatestack = np.zeros((NRUNS, 3))
singlecandidateclass = np.zeros((NRUNS, len(CC_fit)))

for irun in xrange(NRUNS):
    
    # Prepare Data (see mymachinelearning module)
    traindat, traintype, valdat, valtype, checkdat = mml.PrepareML([cluster_fit, compact_fit, random_fit],
                                                               checkdata = CC_fit)
    
    # choose method and create classifier object

    '''
    clf = svm.SVC(C=200.0, cache_size=200, class_weight=None, coef0=0.0,
          decision_function_shape='ovr', degree=3, gamma='auto', kernel='rbf',
          max_iter=-1, probability=True, random_state=None, shrinking=True,
          tol=0.001, verbose=False)
    '''
    clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 4), random_state=1)

    #clf = RandomForestClassifier(max_depth=100, random_state=0)

    '''
    clf =svm.SVC(C=1.0, kernel='rbf', degree=3, gamma='auto', coef0=0.0,
              shrinking=True, probability=False, tol=0.001, cache_size=200,
              class_weight=None, verbose=False, max_iter=-1,
              decision_function_shape='ovr', random_state=None)
    '''
        
    # start learn process
    clf.fit(traindat, traintype)  
    
    # predict train data to verify if learning process worked
    traincheck = clf.predict(traindat)
    # predict data that was not used for learn process to verify
    valcheck = clf.predict(valdat)
    # predict data to classify
    ccclass = clf.predict(checkdat)
        
    # evaluate data
    trainmat = mml.TransitionMatrix(traintype, traincheck)
    valmat = mml.TransitionMatrix(valtype, valcheck)
    
    trainstack[irun] = trainmat
    valstack[irun] = valmat
    singlecandidateclass[irun] = ccclass
    candidatestack[irun] =mml.ClassificationResult(ccclass, np.array((0,1,2)))

# calc mean values 
meantrain = np.mean(trainstack, axis=0)
errtrain = np.sqrt(np.var(trainstack, axis=0))
    
meanval = np.mean(valstack, axis=0)
errval = np.sqrt(np.var(valstack, axis=0))

meancandidate = np.mean(candidatestack, axis=0)
errcandidate = np.sqrt(np.var(candidatestack, axis=0))

# plot mean train and mean validation matrix
plt.figure()
plt.subplot(121)
plt.title('Train Check', fontsize=30)
plt.imshow(meantrain)
mplt.MyBoxText(meantrain*100.)
mplt.MyTransitionMatrix()

plt.subplot(122)
plt.title('Validate', fontsize=30)
plt.imshow(meanval)
mplt.MyBoxText(meanval*100.)
mplt.MyTransitionMatrix(ylabel=False, ytick=True)
plt.show()


if(0):
    hdu=fits.PrimaryHDU(meantrain)
    hdul = fits.HDUList([hdu])
    hdul.writeto('Candidatresults/neurontrainmatrix.fits')
    
    hdu=fits.PrimaryHDU(meanval)
    hdul = fits.HDUList([hdu])
    hdul.writeto('Candidatresults/neuronvalidatematrix.fits')


# plot previously unclassified data results
boxprops=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=.3', alpha=.5)
contentboxprops=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=.3', alpha=1)

barwidth = .8
plt.figure()
plt.title('Cluster Candidates', fontsize=30)
plt.bar([0,1,2], meancandidate, width=barwidth)
plt.xticks([0,1,2],['Cluster', 'Compact', 'Random'], fontsize=20)
mytext='Entries=%d'%np.sum(meancandidate)+'\n'+'Algorithm=??'
numberheight = 30
ax=plt.gca()
plt.text(0.9, 0.9, mytext, fontsize=25,horizontalalignment='right', bbox=boxprops,
         verticalalignment='top', transform=ax.transAxes)

text1 = r'$%.1f$'%meancandidate[0]+'\n'+r'$\pm %.1f$'%errcandidate[0]
text2 = r'$%.1f$'%meancandidate[1]+'\n'+r'$\pm %.1f$'%errcandidate[1]
text3 = r'$%.1f$'%meancandidate[2]+'\n'+r'$\pm %.1f$'%errcandidate[2]

plt.text(0,numberheight,text1, fontsize=25, bbox=contentboxprops,
         horizontalalignment='center', verticalalignment='bottom')
#ax.fill_between([0-barwidth/2.,0.4+barwidth/2.], 50,60, alpha=.5)
plt.text(1,numberheight,text2, fontsize=25, bbox=contentboxprops,
         horizontalalignment='center', verticalalignment='bottom')
plt.text(2,numberheight,text3, fontsize=25, bbox=contentboxprops,
         horizontalalignment='center', verticalalignment='bottom')
plt.yticks(fontsize=20)
plt.show()



probablyclusters = (np.sum(singlecandidateclass, axis=0) == 0)
probablylocs = testloc[probablyclusters]
outputtable = Table(probablylocs, names=('GLON', 'GLAT'))#, meta={'Cluster Candidates':'Machine learning confirmed cluster candidates'})
#outputtable.write('Candidatresults/MLConfirmedOfficialCandidats.fits')

probablynotclusters = np.invert(probablyclusters)
probablynotlocs = testloc[probablynotclusters]
outputnottable = Table(probablynotlocs, names=('GLON', 'GLAT'))
#outputtable.write('Candidatresults/MLRejectedOfficialCandidats.fits', overwrite=True)

