# -*- coding: utf-8 -*-

import os
import healpy as hp
import matplotlib.pyplot as plt

filepath ='data/'

filename = os.listdir(filepath)
files_cleaned =[]

# Select only full maps
for i in filename:
    if 'full' in i:
        files_cleaned.append(i)

filename = files_cleaned

# File number fno
fno = 3
# Plot of a full Planck map
image = hp.fitsfunc.read_map(filepath+filename[fno], h=False)
hp.visufunc.mollview(image, title=filename[fno])
plt.show()


