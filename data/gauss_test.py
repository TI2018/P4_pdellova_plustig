#!/usr/bin/env python2
# -*- coding: utf-8 -*-


import sys
sys.path.insert(0, '../')

import os
import myfunctions as mf
import healpy as hp
import astropy.io.fits as fits
import numpy as np
from time import clock
import matplotlib.pyplot as plt
import myfile as mfl
#import argparse
from mpi4py import MPI


#print os.listdir('.')
#glob_map1 = hp.fitsfunc.read_map('HFI_SkyMap_545_2048_R2.00_halfmission-2.fits', h=False)
#print glob_map1.shape

comm = MPI.COMM_WORLD
rank, size = comm.Get_rank(), comm.Get_size()

#print rank
#print size

Ntries=10
maplength = 50331648
#testmap1 = np.ones(maplength)
#testmap2 = np.ones(maplength)
#np.random.seed(982759815)

position = [6.823754653615293542e+00, -3.471325340323311082e+01]


flux = np.zeros(Ntries)
fluxerr = np.zeros(Ntries)


comm.Barrier()
if rank == 0:
	t0 = clock()

for i in xrange(Ntries):
    testmap1 = np.random.normal(size=maplength)
    testmap2 = np.random.normal(size=maplength)
    flux[i], fluxerr[i] = mf.Photometry_NoBad(testmap1, testmap2, position, 10, 20, 50)
    print flux[i], fluxerr[i]

comm.Barrier()
result = comm.gather(flux,root=0)
result_err = comm.gather(fluxerr, root=0)
comm.Barrier()

if rank==0:
    t = clock()-t0
    print 'calculation time %d s'%t
    result = np.concatenate(result)
    
    result_err = np.concatenate(result_err)
    print len(result)
    filename = 'testresults.txt'
    olddata = np.transpose(np.loadtxt(filename))
    result = np.concatenate([olddata[0], result])
    result_err = np.concatenate([olddata[1], result_err])
    np.savetxt(filename, np.transpose([result, result_err]))
    print np.mean(result)

#testmap2 = np.random.normal(size=maplength)





