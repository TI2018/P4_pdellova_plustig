#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import myfile as mfl
import myfunctions as mfct
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from os import listdir
from time import clock


plt.close("all")

def Fluxfit3Param(frequency, flux, fluxerr, p0=False,returnplot=True):
    '''
    Flux fit for 3 fit parameters.
    input:
        frequence:
            1D numpy.array, is x axis
        flux:
            1D numpy.array, same shape as frequence, is y axis.
        fluxerr:
            1D numpy.array, same shape as frequence, is y error.
        p0:
            optional, array containing 3 start parameter for the fit.
        returnplot:
            bool. If True, create x and y array with input frequency range
            and length to plot the fit and residuals.
    returns:
        coeff:
            array containing the 3 fit coefficients.
        np.sqrt(np.diagonal(pcov)):
            fit coefficient error.
        chi2ndf:
            chi2 per degree of freedom.
        res:
            rediduals. Only returned if returnplot=True
        freqplot:
            xaxis with length 100 to plot. Only returned if returnplot=True
        fitplot:
            yaxis with length 100 to plot, containing fit model. Only returned
            if returnplot=True.
    '''
    
    if not p0:
        coeff, pcov = curve_fit(mfct.Flux_SZ_IR_RAD, frequency, flux, sigma=fluxerr)
    else:
        coeff, pcov = curve_fit(mfct.Flux_SZ_IR_RAD, frequency, flux, sigma=fluxerr,p0=p0)
    fit = mfct.Flux_SZ_IR_RAD(frequency, coeff[0], coeff[1], coeff[2])
    chi2ndf = chisquared(flux, fluxerr, fit) / (len(frequency) - 3.)
    
    if returnplot:
        
        freqplot = np.linspace(min(frequency), max(frequency), 100)
        fitplot = mfct.Flux_SZ_IR_RAD(freqplot, coeff[0], coeff[1], coeff[2])
        res = flux - fit
        return coeff, np.sqrt(np.diagonal(pcov)), chi2ndf, res, freqplot, fitplot
    else:
        return coeff, np.sqrt(np.diagonal(pcov)), chi2ndf
    
    
def chisquared(data, dataerror, model):
    return np.sum(((data-model)/dataerror)**2) 

INFILE = 'ClusterPhotometry.mfl'
INFILE = 'PhotometryResults/CandidateAurelienSamuel.mfl'
OUTFILEPREFIX = 'FitResults/PhotometryFitCandidateAurelienSamuel'


#######################
# unit conversion to MJy
conversion = [244.1, 371.74, 483.690, 287.450, 1., 1.]
freqs = [100., 143., 217., 353., 545., 857.]
delta_nu = [32.9, 45.76, 64.527, 101.4, 171.3, 245.9]
NFREQS = len(freqs)
#######################

## Get Data from File:
data = mfl.read(INFILE)
keys = np.sort(data.keys())
flux = np.zeros((6,len(data[keys[0]])))
fluxerr = np.zeros_like(flux)

# unit conversion
for i in xrange(6):
    flux[i] = data[keys[i+NFREQS]] * conversion[i] # / delta_nu[i]
    fluxerr[i] = data[keys[i]] * conversion[i]     # / delta_nu[i]


flux = np.transpose(flux)
fluxerr = np.transpose(fluxerr)
ntargets = len(flux)

if(1):
    
    # fit for each source in loop
    t0 = clock()
    
    coeff = np.zeros((ntargets, 3))
    coefferr = np.zeros_like(coeff)
    chi2ndf = np.zeros(ntargets)
    
    for i in xrange(ntargets):
        if (i+1)%20==0:
            print 'fitting target %4d of %d'%(i+1, ntargets)
        
        coeff[i], coefferr[i], chi2ndf[i] = Fluxfit3Param(freqs, flux[i], fluxerr[i], returnplot=False)
    
    ttot = clock()-t0
    print 'calculation time : %.1f s'%ttot

    np.savetxt(OUTFILEPREFIX+'.txt', np.transpose([data[keys[-1]], data[keys[-2]], coeff[:,0],coeff[:,1],coeff[:,2]]))
    



if(0):
    # Superpose all datas and fit
    
    superflux = np.sum(flux, axis=0)
    superfluxerr = np.sqrt(np.sum(fluxerr*fluxerr, axis=0))

    
    supercoeff, supercoefferror, superchi2ndf, res, superfitfreq, superfitflux = Fluxfit3Param(freqs, superflux, superfluxerr)#, p0=(200.,2.))
    #supercoeff, supercoefferror, superchi2ndf, res, superfitfreq, superfitflux = Fluxfit2Param(freqs, superflux, .1)
    
    boxtext = r'$\chi^2/\mathrm{ndf}=%.2f$'%superchi2ndf
    boxtext += '\n' + r'$A_{\mathrm{SZ}}=%.2f\pm%.2f$'%(supercoeff[0], supercoefferror[0])
    #boxtext += '\n' + r'$A_{\mathrm{CMB}}=%.2f\pm%.2f$'%(supercoeff[1], supercoefferror[1])
    boxtext += '\n' + r'$A_{\mathrm{IR}}=%.2f\pm%.2f$'%(supercoeff[1], supercoefferror[1])
    boxtext += '\n' + r'$A_{\mathrm{RAD}}=%.2f\pm%.2f$'%(supercoeff[2], supercoefferror[2])
    
    boxprops=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=1')

    errorscale = 1
    
    plt.figure()
    
    plt.subplot2grid((3,3),(0,0), colspan=3, rowspan=2)
    ax1 = plt.gca()
    
    plt.errorbar(freqs, superflux, yerr=superfluxerr*errorscale, linestyle='',
                 markersize=7, marker='o', label=r'$F_{\mathrm{mes}}$')
    plt.plot(superfitfreq, superfitflux, linewidth=5,label=r'$F_{\mathrm{fit}}$')
    xlimit = ax1.get_xlim()
    
    # Plot single contributions
    freqarray = np.linspace(xlimit[0],xlimit[1],100)
    plt.plot(freqarray, mfct.Flux_SZ_IR_RAD(freqarray, supercoeff[0],0,0),
             linestyle='--',label=r'$F_{\mathrm{SZ}}$')
    plt.plot(freqarray, mfct.Flux_SZ_IR_RAD(freqarray, 0, supercoeff[1],0),
             linestyle=':', label=r'$F_{\mathrm{IR}}$')
    plt.plot(freqarray, mfct.Flux_SZ_IR_RAD(freqarray, 0,0, supercoeff[2]),
             linestyle='-.', label=r'$F_{\mathrm{RAD}}$')
    
    plt.text(90., 7000, boxtext, bbox=boxprops, fontsize=20)
    
    ax1.set_xticks([])
    ax1.yaxis.set_tick_params(labelsize=20)
    plt.ylabel(r'$F$ $\left[\frac{\mathrm{MJy}}{\mathrm{Sr}}\right]$', fontsize=25)
    #plt.ylabel('F [MJy Sr$^{-1}$]', fontsize=25)
    plt.legend(fontsize=20, loc=4)
    plt.title('Superflux', fontsize=30, y=1.02)
    
    plt.xlim(xlimit)
    
    
    plt.subplot2grid((3,3),(2,0), colspan=3, rowspan=1)
    plt.errorbar(freqs, res, yerr = superfluxerr*errorscale,linestyle='', marker='o')
    
    ax2 = plt.gca()
    ax2.xaxis.set_tick_params(labelsize=20)
    ax2.yaxis.set_tick_params(labelsize=20)
    
    plt.xlabel('frequency [GHz]', fontsize=25)
    
    #plt.ylabel(r'$F_{\mathrm{mes}}-F_{\mathrm{fit}}$ [MJy Sr$^{-1}$]', fontsize=25)
    plt.ylabel(r'$F_{\mathrm{mes}}-F_{\mathrm{fit}}$ $\left[\frac{\mathrm{MJy}}{\mathrm{Sr}}\right]$', fontsize=25)
    plt.subplots_adjust(hspace=0)
    plt.axhline(color='k')
    plt.xlim(xlimit)
    plt.show()


