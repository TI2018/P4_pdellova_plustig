import numpy as np

def dump(name,array):
    # ToAdd: Matrix
    array=np.array(array)
    sh=array.shape
    Mat=False
    if len(sh)==0:
        sh=[0]
        array=[array]
    if len(sh)==2:
        Mat=True
    elif len(sh)>2:
        print("Matrix Dimension %d too high"%len(sh))
        return
    out="key:\t"+name
    for i in xrange(len(sh)):
        out+="\t"+str(sh[i])
    out+="\n"
    if not Mat:
        out+="\n".join(str(i) for i in array)
        out+="\n"
    else:
        for i in xrange(sh[0]):
            out+="\t".join(str(i) for i in array[i])
            out+="\n"
    return out

def read(filename):
    with open(filename,'r') as infile:
        content=infile.readlines()
    
    # Extract Data from File
    keys=[]
    data=[]
    nkeys=0

    dim=0
    jb=0
    for i in xrange(len(content)):
        line=content[i]
        if line[0]=="#":
            pass
        #elif any(let.isalpha() for let in line):
        elif line[:4]=="key:":
            tmp=line.split("\t")[1:]
            keys.append(tmp[0])
            if len(tmp)==2:
                sh=int(tmp[1])
                if sh==0:
                    dim=0
                else:
                    dim=1
                    jb=i+1
            elif len(tmp)==3:
                sh=(int(tmp[1]),int(tmp[2]))
                dim=2
                jb=i+1
            else:
                print("Error: Shape not given?")
                return
            if dim>0:
                data.append(np.zeros(sh))
            else:
                data.append(0)
            nkeys+=1
        else:
            if dim==1:
                data[nkeys-1][i-jb]=float(line)
            elif dim==0:
                data[nkeys-1]=float(line)
            else:
                tmp=line.split("\t")
                for k in xrange(sh[1]):
                    data[nkeys-1][i-jb][k]=float(tmp[k])
    
    # Write Data in Dictionary
    newdict={}
    for i in xrange(nkeys):
        newdict[keys[i]]=data[i]
    
    return newdict


#array1=[1.,2.,.3]
#float1=3.
#array2=[4.,5.,.6]
#mat0=[array1,array2,array1,array1]
#text=dump("array1",array1)
#text+=dump("float1",float1)
#text+=dump("array2",array2)
#text+=dump("mat0",mat0)
#text+=dump("array2",array2)
#
#with open("outfile.test",'w') as of:
#    of.write(text)
#
#print ReadDict("outfile.test")
