#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import astropy.io.fits as fits
import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
import scipy.constants as sc
import sys

def Photometry_NoBad(GLOBMAP1, GLOBMAP2, CENTER_GCOORD ,RFLUX, R1BACK, R2BACK):
    '''
    Input:
       GLOBMAP1: Healpix-like array
          Planck image (halfmission-1)
       GLOBMAP2: Healpix-like array
          Planck image (halfmission-2)
       CENTER_GCOORD: vector
          Galactic longitude and latitude of the target source
       RFLUX: float
          Radius of the disk in which we integrate the flux of the source
       R1BACK: float
          Inner radius defining the corona in which we compute the background flux
       R2BACK: float
          Outer radius defining the corona in which we compute the background flux
    Returns:
       flux: float
          Photometric flux of the target source
       e_flux: float
          Photometric error on the measure of the flux
    '''
    # Get Index of the first image
    nside = hp.pixelfunc.get_nside(GLOBMAP1)
    
    # Returns an error if the two maps are not fitting
    if hp.pixelfunc.get_nside(GLOBMAP2) != nside:
        print "nsides different"
        return
    
    # Conversion from arcminutes to pixels, using the resolution 1 px = 1.5 '
    RFLUX /= 1.5
    R1BACK /= 1.5
    R2BACK /= 1.5
   
    # Definition of the position of the target
    center = hp.dir2vec(CENTER_GCOORD[0], CENTER_GCOORD[1], lonlat=True)
    # Definition of the target disk
    index_flux = hp.query_disc(nside, center, RFLUX *np.pi/10800.)
    # Definition of the background corona
    index_B1 = hp.query_disc(nside, center, R1BACK *np.pi/10800.)#, inclusive=True)
    index_B2 = hp.query_disc(nside, center, R2BACK *np.pi/10800.)
    index_background = np.setxor1d(index_B1, index_B2)    # Get indexes that are not in both -> they are in background disc
    
    
    flux1 = GLOBMAP1[index_flux]
    flux2 = GLOBMAP2[index_flux]
    
    background1 = GLOBMAP1[index_background]
    background2 = GLOBMAP2[index_background]
    
    # Computation of flux and error on the source
    flux_uncorrected = np.sum(flux1 + flux2)
    e_flux_uncorrected2 = flux1 - flux2
    e_flux_uncorrected2 = np.sum(e_flux_uncorrected2 * e_flux_uncorrected2)
    
    # Computation of flux and error on the background
    B = np.sum(background1 + background2)
    e_B2 = background1 - background2
    e_B2 = np.sum(e_B2 * e_B2)
    
    # Normalization factor between the surface where the flux from the source is computed
    # and the surface where the flux from the background is computed
    NF_NB = float(len(flux1))/float(len(background1))
    
    # Renormalization
    flux = flux_uncorrected - NF_NB * B
    e_flux = np.sqrt( e_flux_uncorrected2 + (NF_NB**2) *  e_B2)
    
    return flux, e_flux


def Blackbody_nu(T, NU):
    '''
    Input:
       T: float
          Temperature of the black body
       NU: float or array
          Frequency
    Returns:
       Theoric flux of a black body of temperature T
    '''
    return (2. * sc.h * (NU **3)) / ((sc.c ** 2) * (np.exp((sc.h * NU) / (sc.k * T)) - 1))

def SZ_mod(T, NU):
    '''
    Input:
       T: float
          Temperature of the black body
       NU: float or array
          Frequency
    Returns:
       Theoric shift of the SZ effect
    '''
    x = (sc.h * NU) / (sc.k * T)
    p1 = x * (np.exp(x)+1) / (np.exp(x)-1) -4.
    p2 = (x ** 4) * np.exp(x) / ((np.exp(x)-1) ** 2)
    return p1 * p2


def Flux_SZ_IR_RAD(NU, ASZ, AIR, ARAD):
    '''
    Input:
       NU: float or array
          Frequency
       ASZ: float
          SZ parameter of the linear model
       ACMB: float
          CMB parameter of the linear model
       AIR: float
          IR parameter of the linear model
       ARAD: float
          RADIO parameter of the linear model
       OFFSET: float
          Offset of the linear model
    Returns:
       F: Flux obtained with that linear model using the input parameters
    '''
    
    # Radiation (alpha) and IR (beta) model parameters
    alpha_r = -0.7
    beta_d = 1.6
    
    # Conversion GHz -> Hz
    NNU = np.copy(NU)
    NNU *= 1.E9
    
    # Modelisation of the SZ emission
    FSZ = SZ_mod(2.725, NNU)
    
    # Modelisation of a modified black body (thermal dust)
    FIR = (NNU ** beta_d) * Blackbody_nu(17., NNU)
    
    # Modelisation of the radio flux emission with a power law
    FRAD = NNU ** alpha_r * 5.E10
    
    # Linear model of the emission
    F = AIR * FIR + ASZ * FSZ + ARAD * FRAD
    
    return F



def progressBar(value, endvalue, bar_length=20):
   '''
   Fancy bar for swag.
   '''

        percent = float(value) / endvalue
        arrow = '-' * int(round(percent * bar_length)-1) + '>'
        spaces = ' ' * (bar_length - len(arrow))

        sys.stdout.write("\rPercent: [{0}] {1}%".format(arrow + spaces, int(round(percent * 100))))
        sys.stdout.flush()

