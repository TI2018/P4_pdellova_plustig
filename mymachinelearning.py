#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 09:18:21 2018

@author: peter
"""

import numpy as np
from sklearn.preprocessing import scale


def PrepareML(learndata, checkdata=None, trainrate=0.8, shuffle=True):
    '''
    Input:
    
    learndata:
        list containing arrays. Each array contains one type. Type is
        defined as learndata index.
    checkdata:
        array-like. data that you want to classify. Will be normalized.
    trainrate:
        float. Rate of learndata that is used for the training process.
        1-trainrate is returned as validation array.
    shuffle:
        bool. If True, leardata arrays are shuffled bevore splitting in
        learn and validation data.
    
    
    Returns:
        
        trainarray, traintype, validate, validatetype, checknormalized
    
    trainarray:
        normalized data that is used for the training process.
    traintype:
        traindata classification, used for the training process.
    validate
        normalized validation data.
    validatetype:
        classification of validation data.
    if checkdata:
        checknormalized:
            array containing normalized data that has to be checked.
    '''
    Ntypes = len(learndata)
    Ntypei = np.zeros(len(learndata), dtype=int)
    Ntypeitrain = np.zeros(len(learndata), dtype=int)
    
    
    for idata in xrange(len(learndata)):
        
        Ntypei[idata] = len(learndata[idata])
        Ntypeitrain[idata] = int(len(learndata[idata]) * trainrate)
        
        if shuffle:
            learndata[idata] = np.random.permutation(learndata[idata])
    
    if checkdata is not None:
        #Ncheck = len(checkdata)
        learndata.append(checkdata)
    
    ######## Verify if right axis !! #######
    allsources = np.concatenate(learndata)
    allsources = scale(allsources, axis=0)
    ######## Verify if right axis !! #######
    
    trainarray = []
    traintype = []
    validate = []
    validatetype = []
    
    for itype in xrange(Ntypes):
        skip = np.sum(Ntypei[0:itype])
        trainarray.append(allsources[skip:skip+Ntypeitrain[itype]])
        traintype.append(np.ones(Ntypeitrain[itype], dtype=int) * itype)
        
        validate.append(allsources[skip+Ntypeitrain[itype]:np.sum(Ntypei)-np.sum(Ntypei[-1:itype:-1])])
        validatetype.append(np.ones(Ntypei[itype]-Ntypeitrain[itype], dtype=int) * itype)
        # If problem check indexing here above, I am not sure...
    
    trainarray = np.concatenate(trainarray, axis = 0)
    traintype = np.concatenate(traintype, axis = 0)
    
    validate = np.concatenate(validate, axis=0)
    validatetype = np.concatenate(validatetype, axis=0)    
    
    if checkdata is not None:
        checknormalized = allsources[np.sum(Ntypei):]
        return trainarray, traintype, validate, validatetype, checknormalized
    else:
        return trainarray, traintype, validate, validatetype



def TransitionMatrix(inputtype, outputtype, norm=True):
    '''
    Input:
        inputtype:
            1D array-like known input type of each sample for learn process.
        outputtype:
            1D array-like. Must have the same shape as inputtype. Per machine 
            learning determined type.
        norm:
            bool. If True, each the matrix lines are normalized.
    returns:
        matrix:
            Transition matrix for each type.
    '''
    
    inputtype=np.array(inputtype)
    outputtype=np.array(outputtype)
    
    typelist = np.unique(inputtype) # is sorted
    matrix = np.zeros((len(typelist),len(typelist)))
    
    for itype in xrange(len(typelist)):
        typemask = inputtype == itype
        tmpresult = outputtype[typemask]
        for iclass in xrange(len(typelist)):
            matrix[itype][iclass] = np.sum(tmpresult==typelist[iclass])
        if norm == True:
            matrix[itype] /= float(np.sum(typemask))
    
    return matrix


def ClassificationResult(classification, classes=None):
    '''
    Input:
        classification: numpy 1D array
            result of the machine learning classification process.
        classes:
            1D array like. Possible classes. If None, classes are defined by
            classes = numpy.arange(min(classification, max(classification)+1, 1))
    Returns:
        classificated:
            1D array-like. Histogram array containing the number of sources
            per classes
        classes:
            only returned if input classes are None. 
    '''
    
    classgiven = classes is not None
    classification = np.array(classification)

    if not classgiven:
        classes = np.arange(min(classification), max(classification)+1, 1)
    
    classificated = np.zeros(len(classes))
    
    for i in xrange(len(classes)):
        classificated[i] = np.sum((classification==classes[i]))
    
    if classgiven:
        return classificated
    else:
        return classificated, classes
