#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 15:56:40 2018

@author: peter
"""

# myplot
import matplotlib.pyplot as plt

def MyBoxText(Matrix):
	'''
	Write value of matrix elements in current matrix imshow plot.
	''' 
    textsize=30
    for i in xrange(3):
        for j in xrange(3):
            plt.text(j,i,'%d%%'% round(Matrix[i][j],0), fontsize=textsize,
                          color='r',
                          horizontalalignment='center',
                          verticalalignment='center')
    

def MyTransitionMatrix(xlabel=True, ylabel=True, xtick = True, ytick=True, xticktop=False):
	'''
	Add labels 'Cluster','Compact S.' and 'Random 'to current plot.
	'''
    ticksize = 20
    labelsize = 25
    ax=plt.gca()
    ax.xaxis.set_tick_params(labelsize=ticksize)
    ax.yaxis.set_tick_params(labelsize=ticksize)
    if xticktop:
        ax.xaxis.tick_top()
    labelpos = [0,1,2]
    labeltext = ['Cluster', 'Compact S.', 'Random']
    labeltext = ['Cl', 'Co', 'Ra']
    
    if xtick:
        plt.xticks(labelpos, labeltext)
    else:
        plt.xticks([])
    
    if ytick:
        plt.yticks(labelpos, labeltext)
    else:
        plt.yticks([])
    
    if xlabel:
        plt.xlabel('Output', fontsize=labelsize)

    if ylabel:
        plt.ylabel('Input', fontsize=labelsize)
        
def DefaultBox(alpha=.5):
	'''
	parameters:
	alpha
	float, opacity
	returns:
	box properties as dictionary.
	'''
    boxprops=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=.3', alpha=.5)
    return boxprops
