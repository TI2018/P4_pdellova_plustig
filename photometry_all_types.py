#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
import myfunctions as mf
import healpy as hp
import astropy.io.fits as fits
import numpy as np
from time import clock
import matplotlib.pyplot as plt
import myfile as mfl

# Target positions
POSITIONS = np.loadtxt('ClusterPositions.txt')
SAVENAME = 'ClusterPhotometry'


filepath ='data/'
filename = os.listdir(filepath)
files_cleaned =[]

npos = POSITIONS.shape[0]

# Map filenames
maphalfnames = [['HFI_SkyMap_100_2048_R2.00_halfmission-1.fits','HFI_SkyMap_100_2048_R2.00_halfmission-2.fits'],
                ['HFI_SkyMap_143_2048_R2.00_halfmission-1.fits','HFI_SkyMap_143_2048_R2.00_halfmission-2.fits'],
                ['HFI_SkyMap_217_2048_R2.00_halfmission-1.fits','HFI_SkyMap_217_2048_R2.00_halfmission-2.fits'],
                ['HFI_SkyMap_353_2048_R2.00_halfmission-1.fits','HFI_SkyMap_353_2048_R2.00_halfmission-2.fits'],
                ['HFI_SkyMap_545_2048_R2.00_halfmission-1.fits','HFI_SkyMap_545_2048_R2.00_halfmission-2.fits'],
                ['HFI_SkyMap_857_2048_R2.00_halfmission-1.fits','HFI_SkyMap_857_2048_R2.00_halfmission-2.fits']]

# Initialization of the photometry results array
phot_data = np.zeros((len(maphalfnames), npos))
phot_data_error = np.zeros_like(phot_data)

# Beams of the instrument in each channel
radfactor = [9.66, 7.27, 5.01, 4.86, 4.84, 4.63]

# Radii for the photometry function at 143 GHz
rs0 = 10.
rb10 = 20.
rb20 = 50.
omega0 = radfactor[1]

# Initialization the computation time
t0 = clock()

# Loading of the maps
for ifreq in xrange(len(maphalfnames)):
    glob_map1 = hp.fitsfunc.read_map(filepath+maphalfnames[ifreq][0], h=False)
    glob_map2 = hp.fitsfunc.read_map(filepath+maphalfnames[ifreq][1], h=False)
    print "loaded map " + maphalfnames[ifreq][0]
    print "and " + maphalfnames[ifreq][1]
    
# Renormalization of the radii for the photometry functions with respect to the beams
    for icluster in xrange(npos):
        rs = rs0 * (omega0 / radfactor[ifreq]) 
        rb1 = rb10 * (omega0 / radfactor[ifreq]) 
        rb2 = rb20 * (omega0 / radfactor[ifreq]) 
# Run of the photometry function for each target
        phot_data[ifreq][icluster], phot_data_error[ifreq][icluster] = mf.Photometry_NoBad(glob_map1, glob_map2,
                 POSITIONS[icluster], rs, rb1, rb2)

# Computation time
ttot = clock()-t0
print 'calculation time : %.1f s'%ttot

#Save of the results
np.savetxt(SAVENAME+'.txt', phot_data)

out = mfl.dump('LON', POSITIONS[:,0])
out += mfl.dump('LAT', POSITIONS[:,1])

out += mfl.dump('FluxPh100GHz', phot_data[0])
out += mfl.dump('FluxPh143GHz', phot_data[1])
out += mfl.dump('FluxPh217GHz', phot_data[2])
out += mfl.dump('FluxPh353GHz', phot_data[3])
out += mfl.dump('FluxPh545GHz', phot_data[4])
out += mfl.dump('FluxPh857GHz', phot_data[5])

out += mfl.dump('ErrorPh100GHz', phot_data_error[0])
out += mfl.dump('ErrorPh143GHz', phot_data_error[1])
out += mfl.dump('ErrorPh217GHz', phot_data_error[2])
out += mfl.dump('ErrorPh353GHz', phot_data_error[3])
out += mfl.dump('ErrorPh545GHz', phot_data_error[4])
out += mfl.dump('ErrorPh857GHz', phot_data_error[5])

with open(SAVENAME+'.mfl', 'w') as outfile:
    outfile.write(out)

#np.savetxt('PhotometryResults.txt', np.transpose(phot_data))
#np.savetxt('PhotometryErrorResults.txt', np.transpose(phot_data_error))


