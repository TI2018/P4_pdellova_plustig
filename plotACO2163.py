#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import os
from matplotlib.ticker import EngFormatter

filepath = './ACO2163_FreqComp/'
print os.listdir(filepath)

#Frequency channels
frequ = [100,143,217,353,545,857]

# Loading of the images
images = []
for i in frequ:
    images.append(np.loadtxt(filepath + 'ACO2163_%dHz.txt'%i))

# Coordinates of ACO2163
centerlon = 6.76831
centerlat = 30.454

# Conversion factor from arcminutes to px
minutes_per_pix = 1.5

imagedim = images[0].shape

# Conversion
leftboarder = 0. - 1.5 * imagedim[1]/2
rightboarder = 0. + 1.5 * imagedim[1]/2

bottomboarder = 0. - 1.5 * imagedim[0]/2
topboarder = 0. + 1.5 * imagedim[0]/2



def PlotProperties(subplot):
    # Definition of tick properties
    ticksize = 20
    tickarray = np.array([-50,-25,0,25,50])
    tickarray = np.array([-30,0,30])

    formatter = EngFormatter(unit = '\'')
    ax=plt.gca()
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_tick_params(labelsize=ticksize)
    ax.xaxis.set_ticks(tickarray)
    
    ax.yaxis.set_major_formatter(formatter)
    ax.yaxis.set_tick_params(labelsize=ticksize)
    ax.yaxis.set_ticks(tickarray)

    
    
plt.figure()

sp1 = plt.subplot2grid((2,3),(0,0), colspan=1, rowspan=1)
plt.title('%d Hz'%frequ[0],fontsize=25)
plt.imshow(images[0], extent=[leftboarder, rightboarder, bottomboarder, topboarder])
PlotProperties(sp1)

sp2 = plt.subplot2grid((2,3),(0,1), colspan=1, rowspan=1)
plt.title('%d Hz'%frequ[1],fontsize=25)
plt.imshow(images[1], extent=[leftboarder, rightboarder, bottomboarder, topboarder])
PlotProperties(sp2)

sp3 = plt.subplot2grid((2,3),(0,2), colspan=1, rowspan=1)
plt.title('%d Hz'%frequ[2],fontsize=25)
plt.imshow(images[2], extent=[leftboarder, rightboarder, bottomboarder, topboarder])
PlotProperties(sp3)

sp4 = plt.subplot2grid((2,3),(1,0), colspan=1, rowspan=1)
plt.title('%d Hz'%frequ[3],fontsize=25)
plt.imshow(images[3], extent=[leftboarder, rightboarder, bottomboarder, topboarder])
PlotProperties(sp4)

sp5 = plt.subplot2grid((2,3),(1,1), colspan=1, rowspan=1)
plt.title('%d Hz'%frequ[4],fontsize=25)
plt.imshow(images[4], extent=[leftboarder, rightboarder, bottomboarder, topboarder])
PlotProperties(sp5)

sp6 = plt.subplot2grid((2,3),(1,2), colspan=1, rowspan=1)
plt.title('%d Hz'%frequ[5],fontsize=25)
plt.imshow(images[5], extent=[leftboarder, rightboarder, bottomboarder, topboarder])
PlotProperties(sp6)

plt.subplots_adjust(bottom=0.05,top=0.95)
plt.show()
