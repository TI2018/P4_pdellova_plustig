import numpy as np
from numpy.random import random_sample
import healpy as hp
import matplotlib.pyplot as plt
import os

# Initialization of the coordinates list
LON = []
LAT = []

# Maximum number of targets to be generated
N = 1000

# Randomly generate N <= 1000 targets uniformly over the sphere, except for |lat| < 30 deg
for k in range(N):
   a, b, c = random_sample()*2 - 1, random_sample()*2 - 1, random_sample()
   lon = a*np.pi
   lat = np.arccos(b)-np.pi/2
   if abs(lat) > np.pi*30./180:
      LON.append(a*np.pi)
      LAT.append(np.arccos(b)-np.pi/2)

# Plot random positions
ax = plt.subplot(111, projection = 'mollweide')
ax.scatter(LON, LAT, marker='.', s=20)
plt.grid()
plt.show()

#Save
np.savetxt('RANDOM_CATALOGUE.txt', np.transpose([LON, LAT]))
