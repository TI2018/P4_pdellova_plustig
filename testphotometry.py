import os
import myfunctions as mf
import healpy as hp
import astropy.io.fits as fits
import numpy as np
import matplotlib.pyplot as plt

# Loading catalogue
with fits.open('catalogues/PSZ2v1.fits') as DF:
    header = DF[0].header.keys
    catalogue = DF[1].data

REDSHIFT = catalogue['REDSHIFT']

GLON = catalogue['GLON']
GLAT = catalogue['GLAT']

CLEANEDGLON = []
CLEANEDGLAT = []

# Keep only targets with positive redshift and with |latitude| > 30 deg
for i in xrange(len(REDSHIFT)):
    if REDSHIFT[i] > 0 and GLAT[i] > 30:
        CLEANEDGLON.append(GLON[i])
        CLEANEDGLAT.append(GLAT[i])

filepath ='data/'

filename = os.listdir(filepath)

def radiusvariation(freq, rmax, N, filename):
   '''
   Input:
      freq: float
         Frequence of the channel
      rmax: float
         Maximum radius to test with the photometry function
      N: integer
         Number of radii to test between 2 and rmax
      filename: string
         Name of the file containing the map used to test the photometry function
   Returns:
      L: list
         List containing the flux returned by the photometry function for each radii
      radius: list
         List containing all the tested radii
   '''

   files_cleaned =[]
   
   # Open catalogue
   for i in filename:
      if str(freq) in i:
         files_cleaned.append(i)

   filename = files_cleaned
   print filename

   # Open map
   image = hp.fitsfunc.read_map(filepath+filename[0], h=False)
   
   # Write a list of radii to be tested by the photometry function
   radius = np.arange(2, rmax, float(rmax)/N)
   ncible = len(CLEANEDGLON)

   # Initialize results list
   L=[0]*len(radius)
   # Stack all available targets from the catalogue
   for n in range(ncible):
      # Run the photometry function for each radii of the list
      for r in range(len(radius)):
         L[r] = L[r] + mf.Photometry_NoBad(image, image, [CLEANEDGLON[n], CLEANEDGLAT[n]], radius[r], radius[r] + 20, radius[r] + 50)[0] / ncible

   return L, radius

L, radius = radiusvariation(143, 100, 10, filename)

plt.scatter(radius, L, marker = '+')
plt.ylim(min(L),max(L))
plt.show()

# Save
#np.savetxt('ACO2163_Photometry/ACO2163_Photometry_Pierre_%dHz.txt'%FREQ, [radius, L])
            


